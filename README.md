# README #

### What is this repository for? ###
This is an implementation of the Flow free game with android. It was a school project. 
Version 1.1

Example:

![Screenshot_2016-05-15-16-37-14.png](https://bitbucket.org/repo/LBKAKp/images/76286265-Screenshot_2016-05-15-16-37-14.png)

### How do I get set up? ###
* Dependencies : open the gradle file at https://bitbucket.org/mamboa/flowfree/src/695701f977d77a29e67a0290bd2fd90f68b45fa7/app/build.gradle?at=master&fileviewer=file-view-default
* Deployment instructions : Clone and run with android Studio.

### Who do I talk to? ###
Repo owner