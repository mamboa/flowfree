package tp1inf80405.flowfree.Terrain;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import tp1inf80405.flowfree.Activities.PlayActivity;
import tp1inf80405.flowfree.PartieJouable.Jeu;

/**
 * Cette case:
 * - dessine une grille
 * - dÃ©finit aussi les mÃ©caniques de jeu: effets visuels, la logique
 * de remplissage des cases, les mouvements du doigt de l'utilisateur etc.
 * Created by TP1 on 28/01/2016.
 */
public class Grille extends View {

    //couleur d'une qui est visitÃ© par le curseur ou qui contient un bout chemin
    private Paint caseActive_ = new Paint();
    //un chemin normal avec sa couleur
    private Paint cheminColore_ = new Paint();
    //ombre d'un chemin colore
    private Paint ombreColore_ = new Paint();
    //un point normal avec sa couleur
    private Paint pointColore_ = new Paint();
    //l'ombre d'un point colore/actif
    private Paint cercleColore_ = new Paint();

    // activite courante
    private Activity activity_;
    // Dimensions de la surface de jeu et de cases (largeur et hauteur)
    private int dimensions_;
    private int largeurCase_;
    private int hauteurCase_;
    private int nombreTotalCases_;

    //la liste des chemins
    private ArrayList<Chemin> lesChemins_;
    //la liste des points
    private ArrayList<Point> lesPoints_;
    //point(cercle) sur lequel le doigt est posÃ©
    private Case pointPresse = null;
    //chemin en cours de dessin
    private Chemin cheminActif_ = null;
    //liste des cases sur la grille
    private ArrayList<Case> listeCases_;

    //liste de couleurs
    private ArrayList<Integer> listeCouleurs;

    public Grille(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity_ = (Activity)getContext();
        //aspect remplissage
        caseActive_.setStyle(Paint.Style.STROKE);
        caseActive_.setColor(Color.BLACK);
        //aspect bouton
        pointColore_.setStyle(Paint.Style.FILL);
        cheminColore_.setStyle(Paint.Style.STROKE);
        //aspect du chemin
        cheminColore_.setStrokeCap(Paint.Cap.ROUND);
        cheminColore_.setStrokeJoin(Paint.Join.ROUND);
        cheminColore_.setAntiAlias(true);
        ombreColore_.setStyle(Paint.Style.FILL);
        cercleColore_.setStyle(Paint.Style.FILL);

        nombreTotalCases_ = 0;
        listeCouleurs = new ArrayList<Integer>();
        listeCases_ = new ArrayList<Case>();
    }

    /**
     * initialiserJeu
     * installe une partie sur une grille
     * @param jeu le niveau Ã  installer sur la grille
     */
    public void initialiserJeu(Jeu jeu) {
        dimensions_ = jeu.obtenirTaille();
        nombreTotalCases_ = dimensions_ * dimensions_;
        if(listeCases_.size()!=0)
            listeCases_.clear();
        chargerLesCases(dimensions_);
        lesPoints_ = new ArrayList<Point>();
        lesPoints_.addAll(jeu.obtenirPoints());
        lesChemins_ = new ArrayList<Chemin>();
        for(int i = 0; i < jeu.obtenirNombreCouleurs(); i++)
            lesChemins_.add(new Chemin(jeu.obtenirPoints().get(2*i).obtenirIndiceCouleur()));
    }

    @Override
    protected void onSizeChanged(int a, int b, int x, int y) {
        //epaisseur du trait
        int trait = Math.max(1, (int) caseActive_.getStrokeWidth());
        largeurCase_ = (a - (this.getPaddingRight() + this.getPaddingLeft() + trait)) / dimensions_;
        hauteurCase_ = (b - (this.getPaddingTop() + this.getPaddingBottom() + trait)) / dimensions_;
        //on redefinit l'epaisseur du trait
        cheminColore_.setStrokeWidth((float)largeurCase_/4);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Ici on dessine la grille en plusieurs petis rectangles
        Rect rect = new Rect();
        //i parcourt les lignes
        for (int i = 0; i < dimensions_; i++) {
            //j parcourt les colonnes
            for (int j = 0; j < dimensions_; j++) {
                int x = conversionGrilleVersEcranX(j);
                int y = conversionGrilleVersEcranY(i);
                rect.set(x, y, x + largeurCase_, y + hauteurCase_);
                canvas.drawRect(rect, caseActive_);
            }
        }
        int i;
        // ensuite on dessine les points
        for (i = 0; i < lesPoints_.size(); i++)
            dessinerPoint(canvas,lesPoints_.get(i));

        // on ajoute les couleurs sur les cases actives
        for(i=0; i < lesChemins_.size(); i++)
            if (lesChemins_.get(i) != cheminActif_)
                dessinerChemin(canvas, lesChemins_.get(i));

        // on dessine les cases actives vides
        dessinerChemin(canvas, cheminActif_);
        dessinerCercleAutourDuDoigt(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int largeur = this.getMeasuredWidth() - (this.getPaddingLeft() + this.getPaddingRight());
        int hauteur = this.getMeasuredHeight() - (this.getPaddingTop() + this.getPaddingBottom());
        int size = Math.min(hauteur, largeur);
        //nouvelle dimensions de l'image
        this.setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size + getPaddingTop() + getPaddingBottom());
    }

    //Ã©vÃ¨nements sur l'Ã©cran (gestures)
    //---> dÃ©but
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        int colonne = conversionEcranVersGrilleX(x);
        int ligne = conversionEcranVersGrilleY(y);

        if(colonne >= dimensions_ || ligne >= dimensions_ || colonne < 0 || ligne < 0) {
            if(cheminActif_ != null) {
                miseAJourCheminInComplet(cheminActif_);
                afficherLeCheminActif();
                if(activity_.getClass().equals(PlayActivity.class)) {
                    ((PlayActivity) activity_).update();
                }
            }
            pointPresse = null;
            //redessine
            invalidate();
            //redessine
            if(activity_.getClass().equals(PlayActivity.class)) {
                ((PlayActivity) activity_).getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
            }
            return true;
        }

        Case uneCase = new Case(colonne, ligne);
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                Chemin chemin = obtenirLeCheminDuneCase(uneCase, true);
                if(chemin != null) {
                    if(siPointPresent(uneCase)) {
                        chemin.reinitialiser();
                        chemin.ajouter(new Case(colonne, ligne));
                        miseAJourCheminInComplet(chemin);
                        if(activity_.getClass().equals(PlayActivity.class)) {
                            ((PlayActivity) activity_).mAJCases();
                        }
                    }
                    else {
                        chemin.retirerUneCase(uneCase);
                        if(activity_.getClass().equals(PlayActivity.class)) {
                            ((PlayActivity) activity_).mAJCases();
                        }
                    }
                    cheminActif_ = chemin;
                    pointPresse = new Case(x, y);
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if(cheminActif_ != null) {
                    pointPresse = new Case(x, y);
                    if(!uneCase.equals(cheminActif_.obtenirLaDerniereCase()) && !cheminActif_.siVide()) {
                        //si la case est adjacente Ã  la derniere du chemin
                        if(actionValide(uneCase)) {
                            //retire une case du chemin si elle est déjà présente dans le chemin
                            if(cheminActif_.contains(uneCase)) {
                                cheminActif_.retirerUneCase(uneCase);
                                miseAJourCheminInComplet(cheminActif_);
                                if(activity_.getClass().equals(PlayActivity.class)) {
                                    ((PlayActivity) activity_).mAJCases();
                                }
                            }
                            //ajoute une case au chemin si elle n'est pas dans le chemin
                            else {
                                cheminActif_.ajouter(uneCase);
                                if(activity_.getClass().equals(PlayActivity.class))
                                    ((PlayActivity) activity_).mAJCases(); //mets Ã  jour l'ecran

                                //si la case contient le dernier point
                                Point pointFinal = obtenirLePointDansLaCase(uneCase);
                                if(pointFinal != null && !uneCase.equals(cheminActif_.obtenirLaPremiereCase())) {
                                    miseAJourCheminInComplet(cheminActif_);
                                    afficherLeCheminActif();
                                    miseAJourCheminComplet(pointFinal);
                                    if(activity_.getClass().equals(PlayActivity.class)) {
                                        ((PlayActivity) activity_).update();
                                    }
                                    pointPresse = null;
                                }
                            }
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if(cheminActif_ != null) {
                    miseAJourCheminInComplet(cheminActif_);
                    afficherLeCheminActif();
                    if(activity_.getClass().equals(PlayActivity.class))
                        ((PlayActivity) activity_).update();
                }
                break;
        }

        //redessine
        invalidate();
        if(activity_.getClass().equals(PlayActivity.class)) {
            ((PlayActivity) activity_).getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
        }
        return true;
    }
    //---> fin


    /**
     * dessinerPoint
     * dessine un point en forme de cercle
     * @param canvas le canvas en cours
     * @param point le point Ã  dessiner sur la grille
     */
    private void dessinerPoint(Canvas canvas, Point point) {
        pointColore_.setColor(listeCouleurs.get(point.obtenirIndiceCouleur()));
        Case c = point.obtenirCase();
        canvas.drawCircle(conversionGrilleVersEcranX(c.obtenirColonne()) + (largeurCase_ / 2), conversionGrilleVersEcranY(c.obtenirLigne()) + (hauteurCase_ / 2),
                largeurCase_ / 3, pointColore_);
    }

    /**
     * dessinerChemin
     * dessine sur la grille le chemin dÃ©finit par l'utilisateur
     * @param canvas le canvas en cours
     * @param unChemin le chemin dÃ©finit par le parcour des doigts Ã  l'Ã©cran
     */
    private void dessinerChemin(Canvas canvas, Chemin unChemin) {
        if(unChemin != null) {
            if (!unChemin.siVide()) {
                if(unChemin != cheminActif_)
                    for(Case c : unChemin.obtenirLeChemin())
                        //ici on dessine un rectangle de couleur legerement pale pour le rendu visuel
                        dessinerOmbreCase(canvas, c, unChemin);

                // dessiner le chemin en tant que tel
                Path path = new Path();
                List<Case> listeCase = unChemin.obtenirLeChemin();
                Case caseTouchee = listeCase.get(0);
                path.moveTo(conversionGrilleVersEcranX(caseTouchee.obtenirColonne()) + largeurCase_ / 2,
                        conversionGrilleVersEcranY(caseTouchee.obtenirLigne()) + hauteurCase_ / 2);

                for(int i = 0; i < listeCase.size(); i++) {
                    if(i > 0) {
                        caseTouchee = listeCase.get(i);
                        path.lineTo(conversionGrilleVersEcranX(caseTouchee.obtenirColonne()) + largeurCase_ / 2,
                                conversionGrilleVersEcranY(caseTouchee.obtenirLigne()) + hauteurCase_ / 2);
                    }
                    if(cheminActif_ != null) {
                        if(i < listeCase.size() - 1) {
                            Case caseSuivante = listeCase.get(i + 1);
                            if(unChemin != cheminActif_ && cheminActif_.contains(caseSuivante)) {
                                break;
                            }
                        }
                    }
                }
                cheminColore_.setColor(listeCouleurs.get(unChemin.obtenirIndiceCouleur()));
                canvas.drawPath(path, cheminColore_);
            }
        }
    }

    /**
     * dessinerOmbreCase
     * dessine une ombre sur une case (garantit un effet visuel) active
     * c'est Ã  dire dont qui a soit un point soit un desssin Ã  l'interieur
     * @param canv le canvas
     * @param uneCase la case courante
     * @param unChemin le chemin courant
     */
    private void dessinerOmbreCase(Canvas canv, Case uneCase, Chemin unChemin){
        Rect rect = new Rect();
        int x = conversionGrilleVersEcranX(uneCase.obtenirColonne());
        int y = conversionGrilleVersEcranY(uneCase.obtenirLigne());
        rect.set(x, y, x + largeurCase_, y + hauteurCase_);
        ombreColore_.setColor(listeCouleurs.get(unChemin.obtenirIndiceCouleur()));
        ombreColore_.setAlpha(50);
        canv.drawRect(rect, ombreColore_);
    }

    /**
     * dessinerCercleAutourDuDoigt
     * lorsque le doigt est dans une case quelconque, cette methode garantie un feed-back visuel
     * @param canvas le canvas
     */
    private void dessinerCercleAutourDuDoigt(Canvas canvas) {
        if(pointPresse != null && cheminActif_ != null) {
            Rect newRect = canvas.getClipBounds();
            newRect.inset(-largeurCase_, -largeurCase_);  //make the rect larger
            canvas.clipRect(newRect, Region.Op.REPLACE);
            cercleColore_.setColor(listeCouleurs.get(cheminActif_.obtenirIndiceCouleur()));
            cercleColore_.setAlpha(50);
            canvas.drawCircle(pointPresse.obtenirColonne(), pointPresse.obtenirLigne(), largeurCase_, cercleColore_);
        }
    }

    /**
     * permet de charger les cases suivant les dimensions choisies
     * @param dimensions dimension du terrain
     */
    public void chargerLesCases(int dimensions){
        for(int i = 0; i < dimensions ; i++)
            for(int j = 0; j < dimensions ; j++)
                listeCases_.add(new Case(i,j));
    }

    /**
     * lorsque deux points de mÃªme couleurs sont reliÃ©s par un chemin, cette methode met a jour la l'Ã©tat du
     * chemin concerne Ã  jour dans la liste de chemins
     * @param cloture le point de cloture
     */
    public void miseAJourCheminComplet(Point cloture){
        if (cloture != null) {
            for (int i = 0; i < lesChemins_.size(); i++) {
                if (lesChemins_.get(i).obtenirIndiceCouleur() == cloture.obtenirIndiceCouleur())
                    lesChemins_.get(i).assignerTermine(true);
            }
        }
    }

    /**
     * si le chemin actif est dÃ©ja complet, effacer le dessin diminue le nombre de tubes formÃ©s
     * @param cheminActif
     */
    public void miseAJourCheminInComplet(Chemin cheminActif){
        if(cheminActif != null) {
            for (int i = 0; i < lesChemins_.size(); i++) {
                if (lesChemins_.get(i).obtenirIndiceCouleur() == cheminActif.obtenirIndiceCouleur())
                    if (lesChemins_.get(i).obtenirTermine() == true)
                        lesChemins_.get(i).assignerTermine(false);
            }
        }
    }

    /**
     * retourne le nombre de tubes formÃ©s
     * @return nombre de tubes
     */
    public int nombreTubesFormes(){
        int nombre = 0;
        for(int i = 0; i < lesChemins_.size(); i++)
            if(lesChemins_.get(i).obtenirTermine() == true)
                nombre++;
        return nombre;
    }

    /**
     * nombreDeCasesOccupees
     * retourne le nombre de cases occupÃ©es Ã  un moment donnÃ©
     * @return nombreCase
     */
    public int nombreDeCasesOccupees() {
        int nombreCase = 0;
        detecterOccupationDesCases();
        for(int i = 0 ; i < listeCases_.size(); i++)
            if(listeCases_.get(i).obtenirNombrePassage() >= 1)
                 nombreCase++;
        return nombreCase;
    }

    /**
     *cette methode fouille chaque case de chaque chemin (de la liste des chemins)
     * lorsqu'une case correspond à une case sur la grille son nombre de passage est
     * incremente
     */
    public void detecterOccupationDesCases(){
        //initialise
        for(int i=0; i < listeCases_.size(); i++)
            listeCases_.get(i).assignerNombrePassage(0);

        //pour chaque case de la grille
        for(int i =0; i < listeCases_.size() ;i++){
            //on parcour chaque chemin
            for (int j = 0 ; j < lesChemins_.size(); j++){
                //on parcour chaque case
                for(int k = 0; k < lesChemins_.get(j).obtenirLeChemin().size(); k++){
                    if( listeCases_.get(i).equals(lesChemins_.get(j).obtenirLeChemin().get(k)))
                        listeCases_.get(i).incrementerNombrePassage();
                }
            }
        }
    }

    //conversion des coordonnÃ©es de l'Ã©cran --->DÃ©but
    /**
     * conversionEcranVersGrille
     * pour une abscisse x d'une coordonnÃ©e de pixel Ã  l'Ã©cran, donne la colonne de
     * la coordonnÃ©e de la grille Ã  laquelle elle appartient.
     * @param x la coordonnÃ©e x
     * @return colonne
     */
    private int conversionEcranVersGrilleX(int x) {
        return (int)Math.floor((float)(x - this.getPaddingLeft()) / largeurCase_);
    }

    /**
     * conversionEcranVersGrilleY
     * pour une ordonnÃ©e y d'une coordonnÃ©e de pixel Ã  l'Ã©cran, donne la ligne de
     * la coordonnÃ©e de la grille Ã  laquelle elle appartient.
     * @param y la coordonneÃ© y
     * @return ligne
     */
    private int conversionEcranVersGrilleY(int y) {
        return (int)Math.floor((float)(y - this.getPaddingTop()) / hauteurCase_);
    }

    /**
     * conversionGrilleVersEcranX
     * pour une colonne quelconque dans la grille, donne la composante x de son abscisse
     * dans le rÃ©fÃ©rentiel des pixels
     * @param colonne la composante colonne dans la grille
     * @return x
     */
    private int conversionGrilleVersEcranX(int colonne) {
        return  this.getPaddingLeft() + (largeurCase_ * colonne);
    }

    /**
     * conversionGrilleVersEcranX
     * pour une colonne quelconque dans la grille, donne la composante x de son abscisse
     * dans le rÃ©fÃ©rentiel des pixels
     * @param ligne la composante ligne dans la grille
     * @return y
     */
    private int conversionGrilleVersEcranY(int ligne) {
        return this.getPaddingTop() + (hauteurCase_ * ligne) ;
    }
    //fin des conversions -->

    /**
     * siVoisins
     * DÃ©termine si deux cases sont voisines
     * @param c1 la premiere case
     * @param c2 la deuxieme case
     * @return vrai si oui, faux sinon
     */
    private boolean siVoisins(Case c1, Case c2) {
        return Math.abs(c1.obtenirColonne() - c2.obtenirColonne()) + Math.abs(c1.obtenirLigne() - c2.obtenirLigne()) == 1;
    }

    /**
     * obtenirLePointDansLaCase
     * permet d'obtenir un point contenu dans une case
     * @param uneCase la case d'ont on desire avoir le point
     * @return le point s'il existe, null sinon
     */
    @Nullable
    private Point obtenirLePointDansLaCase(Case uneCase) {
        int i;
        int taille  = lesPoints_.size();
        for(i = 0; i < taille; i++)
            if(lesPoints_.get(i).obtenirCase().equals(uneCase))
                return lesPoints_.get(i);
        return null;
    }

    /**
     * obtenirCheminViaCouleur
     * obtenir le chemin dont la couleur est donnÃ©e en entrÃ©e
     * @param idCouleur l'identifiant de couleur
     * @return le chemin
     */
    private Chemin obtenirCheminViaCouleur(int idCouleur) {
        for (int i = 0 ; i<lesChemins_.size();i++)
            if (idCouleur == lesChemins_.get(i).obtenirIndiceCouleur())
                return  lesChemins_.get(i);
        return null;
    }

    /**
     * obtenirLeCheminDuneCase
     * pour une case bien donnÃ©e on obtient le chemin auquel elle appartient presentement
     * @param uneCase la case dont on veut obtenir le chemin
     * @param siActive si la case est active
     * @return le chemin s'il existe
     */
    @Nullable
    private Chemin obtenirLeCheminDuneCase(Case uneCase, boolean siActive) {
        //si une case est dans le chemin on retourne ce chemin
        int taille = lesChemins_.size();
        int i;
        for(i = 0 ; i < taille; i++)
            if(!(siActive && lesChemins_.get(i) == cheminActif_)) {
                List<Case> lesCases = lesChemins_.get(i).obtenirLeChemin();
                if(lesCases.contains(uneCase))
                    return lesChemins_.get(i);
            }

        //si une case contient un point, on commence un chemin pour cette couleur
        Point point = obtenirLePointDansLaCase(uneCase);
        if(point != null) {
            int idCouleur = point.obtenirIndiceCouleur();
            Chemin chemin = obtenirCheminViaCouleur(idCouleur);
            if(!(chemin == cheminActif_ && siActive))
                return chemin;
        }
        return null;
    }

    /**
     * siPointPrsent()
     * determine si une case contient un point
     * @param uneCase la case qu'on veut vÃ©rifier
     * @return true si un point est prÃ©sent dans cette case, non sinon
     */
    private boolean siPointPresent(Case uneCase) {
        int i;
        int taille = lesPoints_.size();
        for(i = 0; i < taille ;i++)
            if(lesPoints_.get(i).obtenirCase().equals(uneCase))
                return true;
        return false;
    }

    /**
     *  cette methode dessine le chemin au fur et a mesure que le doigt de l'utilisateur
     *  parcourt les cases
     */
    private void afficherLeCheminActif() {
        if(cheminActif_ != null) {
            for(Case c : cheminActif_.obtenirLeChemin()) {
                if(activity_.getClass().equals(PlayActivity.class)) {
                    ((PlayActivity) activity_).mAJCases();
                }
            }
            cheminActif_ = null;
        }
    }

    /**
     * reinitialiser tous les chemins
     */
    public void reinitialiser() {
        for(int i = 0;  i < lesChemins_.size(); i++) {
            //la ligne ci-dessous aide Ã  mettre Ã  jour le nombre de tubes
            lesChemins_.get(i).assignerTermine(false);
            lesChemins_.get(i).reinitialiser();
        }

        for(int i=0; i < listeCases_.size(); i++)
            listeCases_.get(i).assignerNombrePassage(0);
        cheminActif_ = null;
        pointPresse = null;
    }

    /**
     * siVictoire
     * si une seule case de la grille a plus de 2 passages de chemin Ã  la fin de la partie
     * elle est considerÃ©e comme perdue
     * @return
     */
    public boolean siVictoire(){
        for(int i = 0; i < listeCases_.size(); i++)
            if (listeCases_.get(i).obtenirNombrePassage() > 1)
                return false;
        return true;
    }

    /**
     * siTermine
     * si tous les tubes sont formes et que toutes les cases on Ã©tÃ© occupees
     * la partie est terminees
     * @return si une partie est terminee ou pas
     */
    public boolean siTermine(){
        if((nombreTubesFormes() == (lesPoints_.size() / 2)) && (nombreDeCasesOccupees() == nombreTotalCases_))
            return true;
        return false;
    }

    /**
     * actionValide(Case uneCase): determine si une action est valide
     * une action n'est valide que si elle est effectuÃ©e entre deux cases adjacentes
     * utilisÃ© pour ajouter une case au chemin total
     * @param uneCase la case voisine
     * @return oui si l'action est valide
     */
    private boolean actionValide(Case uneCase) {
        //si ils ne sont pas adjacents
        if(!siVoisins(uneCase, cheminActif_.obtenirLaDerniereCase()))
            return false;
        //si la case contient un point d'une autre couleur
        int i;
        int taille = lesPoints_.size();
        for(i = 0; i < taille ;i++)
            if(cheminActif_.obtenirIndiceCouleur() != lesPoints_.get(i).obtenirIndiceCouleur()&& lesPoints_.get(i).obtenirCase().equals(uneCase))
                return false;
        return true;
    }

    /**
     *chargerLesCouleurs()
     */
    public void chargerLesCouleurs() {
        int BLUE1 =  Color.parseColor("#0070C0");
        int RED1 =   Color.parseColor("#FF0000");
        int GREEN1 = Color.parseColor("#00B050");
        int YELLOW = Color.parseColor("#FFFF0C");
        int ORANGE = Color.parseColor("#E36C0A");
        int BLUE2 =  Color.parseColor("#C4EEF3");
        int GREEN2 = Color.parseColor("#93FF99");
        int RED2 =   Color.parseColor("#943634");
        int GRAY =   Color.parseColor("#938953");
        listeCouleurs.add(BLUE1);
        listeCouleurs.add(RED1);
        listeCouleurs.add(GREEN1);
        listeCouleurs.add(YELLOW);
        listeCouleurs.add(ORANGE);
        listeCouleurs.add(BLUE2);
        listeCouleurs.add(GREEN2);
        listeCouleurs.add(RED2);
        listeCouleurs.add(GRAY);
    }
}