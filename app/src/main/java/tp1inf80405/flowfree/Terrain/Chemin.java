package tp1inf80405.flowfree.Terrain;

import java.util.ArrayList;
import java.util.List;

/**
 * Chemin
 * cette classe définit le chemin en terme cases parcourues par l'utilisateur
 */
public class Chemin {

    private ArrayList<Case> chemin_;
    private int indiceCouleur_;
    private boolean termine_;

    /**
     * constructeur
     * @param indiceCouleur : l'indice de la couleur
     */
    public Chemin(int indiceCouleur) {
        indiceCouleur_ = indiceCouleur;
        chemin_= new ArrayList<Case>();
        termine_ = false;
    }

    /**
     * obtenirIndiceCouleur()
     * @return indiceCouleur_
     */
    public int obtenirIndiceCouleur() {
        return indiceCouleur_;
    }

    /**
     * reinitialiser()
     * efface le chemin
     */
    public void reinitialiser() {
        chemin_.clear();
    }

    /**
     * ajouter(Case uneCae)
     * ajoute une case au chemin
     * @param uneCase: la case a ajouter
     */
    public void ajouter(Case uneCase) {
        chemin_.add(uneCase);
    }

    /**
     * obtenirLeChemin()
     * retourne le chemin qui est constitué d'une liste de cases
     */
    public List<Case> obtenirLeChemin() {
        return chemin_;
    }

    /**
     * obtenirLaPremiereCase()
     * obtenir la premiere case du chemin
     * @return null
     */
    public Case obtenirLaPremiereCase() {
        if(!siVide()) {
            return chemin_.get(0);
        }
        return null;
    }

    /**
     * obtenirLaDerniereCase()
     * @return chemin_
     */
    public Case obtenirLaDerniereCase() {
        return chemin_.get(chemin_.size() - 1);
    }

    /**
     * siVide()
     * détermine si le chemin est vide
     * @return null
     */
    public boolean siVide() {
        return chemin_.isEmpty();
    }

    /**
     * retirerUneCase()
     * retirer une case donnée du chemin
     * @param uneCase: la case à retirer
     */
    public void retirerUneCase(Case uneCase) {
        int indice = chemin_.indexOf(uneCase);
        if(indice >= 0) {
            for (int i = chemin_.size()-1; i > indice; --i) {
                chemin_.remove(i);
            }
        }
    }

    /**
     * retirerCasePrecedente()
     * @param uneCase: case prise comme point de depart
     */
    public void retirerCasePrecedente(Case uneCase) {
        int indice = chemin_.indexOf(uneCase);
        if(indice > 0) {
            retirerUneCase(chemin_.get(indice - 1));
        }
    }

    /**
     * taille()
     * retourne le nombre de cases contenues dans un chemin
     * @return taille
     */
    public int taille() {
        return chemin_.size();
    }

    /**
     * Ccontains()
     * permet de si un chemin contient une case bien précise
     * @param uneCase
     * @return oui sit tel est le cas
     */
    public boolean contains(Case uneCase) {
        return obtenirLeChemin().contains(uneCase);
    }

    public boolean obtenirTermine(){    return  termine_; }
    public void assignerTermine(boolean fini){ termine_ = fini; }
}
