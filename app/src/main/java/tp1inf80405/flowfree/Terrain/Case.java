package tp1inf80405.flowfree.Terrain;

/**
 * Case
 * Cette classe, définit une case.
 * Une case peut être vide à l'origine, dans ce cas elle peut servir de chemin
 * Une case peut contenir un point d'une couleur quelconque.
 * la grille principale affichée à l'image est constituée d'un ensemble de cases.
 * Created by TP1 on 28/01/2016.
 */
public class Case {

    private int ligne_;
    private int colonne_;
    private int nombrePassages_;

    public Case(int colonne, int ligne) {
        ligne_ = ligne;
        colonne_ = colonne;
        nombrePassages_ = 0;
    }

    //quelques accesseurs
    public int obtenirLigne() {
        return ligne_;
    }
    public int obtenirColonne() {
        return colonne_;
    }
    public int obtenirNombrePassage(){ return  nombrePassages_; }
    //permet de savoir combien de chemins passent par cette case
    public void assignerNombrePassage(int nombrePassages) { nombrePassages_ = nombrePassages;}
    public void incrementerNombrePassage() {nombrePassages_++;}
    public void decrementerNombrePassage() {nombrePassages_--;}

    /**
     * equals
     * determiner si la case courante est égale à celle passée en paramètre
     * ici on a surcharger la fonction equals de java
     * @param tampon
     * @return
     */
    @Override
    public boolean equals(Object tampon) {
        if(!(tampon instanceof Case)) {
            return false;
        }
        Case tampon1 = (Case) tampon;
        return tampon1.obtenirColonne() == this.obtenirColonne() && tampon1.obtenirLigne() == this.obtenirLigne();
    }
}
