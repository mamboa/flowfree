package tp1inf80405.flowfree.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by TP1 on 07/02/2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "LesParties";
    public static final int DB_VERSION = 2;

    public static final String TableParties = "parties";
    public static final String[] TablePartiesCols = { "partieId","termine"};

    private static final String sqlCreateTableParties =
            "CREATE TABLE parties(" +
                    " partieId INTEGER PRIMARY KEY NOT NULL," +
                    " termine INTEGER " +
                    ");";

    private static final String sqlDropTableParties =
            "DROP TABLE IF EXISTS parties;";


    public DbHelper( Context context ) {
        super( context, DB_NAME, null, DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateTableParties);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(sqlDropTableParties);
        onCreate( db );
    }

    public boolean siTermine(int id) {
        String query = "SELECT  * FROM parties WHERE partieId =" + id ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int returnValue = 0;
        if (cursor.moveToFirst()) {
            do {
                returnValue = Integer.parseInt(cursor.getString(1));
            } while (cursor.moveToNext());
        }

       if(returnValue == 0)
           return false;
        return true;
    }
}