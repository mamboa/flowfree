package tp1inf80405.flowfree.Activities.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import tp1inf80405.flowfree.Activities.Adapters.ButtonAdapter;
import tp1inf80405.flowfree.PartieJouable.SingletonJeu;
import tp1inf80405.flowfree.R;

/**
 * Created by TP1 on 07/02/2016.
 */
public class Fragment8 extends Fragment {

    private SingletonJeu singletonJeu_ = SingletonJeu.getInstance();
    private ButtonAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gridactivityfragment, container, false);
        Context context = getActivity();

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        mAdapter = new ButtonAdapter(context, singletonJeu_.taille8,8);
        gridview.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }


}