package tp1inf80405.flowfree.Activities.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

import tp1inf80405.flowfree.Activities.PlayActivity;
import tp1inf80405.flowfree.DB.DbHelper;
import tp1inf80405.flowfree.PartieJouable.SingletonJeu;


public class ButtonAdapter extends BaseAdapter {
    private Context context_;
    private int nombre_;
    private int taille_;

    private SingletonJeu singletonJeu_ = SingletonJeu.getInstance();
    private DbHelper dbHelper;

    public ButtonAdapter(Context cont,int nombre, int taille) {
        context_ = cont;
        nombre_ = nombre;
        taille_ = taille;
        dbHelper = new DbHelper(cont);
    }

    public int getCount() {
        return nombre_;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Button button;
        if (convertView == null) {
            button = new Button(context_);
            button.setLayoutParams(new GridView.LayoutParams(200, 200));
            button.setPadding(10, 10, 10, 10);
            switch (taille_) {
                case 7:
                    button.setId(position);
                    break;
                case 8:
                    button.setId(position + singletonJeu_.taille7);
                    break;
            }
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context_, PlayActivity.class);
                    intent.putExtra("Id", v.getId());
                    context_.startActivity(intent);
                }
            });

        } else {
            button = (Button) convertView;
        }

        int victoire =  Color.parseColor("#FF09B707");
        int disponible = Color.parseColor("#FF59C1D0");
        if (dbHelper.siTermine(button.getId())) {
            button.setBackgroundColor(victoire);
        } else {
            button.setBackgroundColor(disponible);
        }

        //si la partie prcedente n'est pas terminee la partie en cours est inaccessible
        //sauf la première partie de chaque niveau
        if(button.getId()>0) {
            if (!dbHelper.siTermine(button.getId() - 1)) {
                button.setEnabled(false);
                button.setBackgroundColor(Color.GRAY);
            }
            else
                button.setEnabled(true);
        }

        if(button.getId() == 0) {
            if (!dbHelper.siTermine(button.getId())) {
                button.setEnabled(true);
                button.setBackgroundColor(disponible);
            }
        }
        if(button.getId() == singletonJeu_.taille7) {
            if (!dbHelper.siTermine(button.getId())) {
                button.setEnabled(true);
                button.setBackgroundColor(disponible);
            }
        }

        if (button.getId()> singletonJeu_.taille7){
            if (!dbHelper.siTermine(button.getId() - 1)) {
                button.setEnabled(false);
                button.setBackgroundColor(Color.GRAY);
            }
            else
                button.setEnabled(true);
        }

         button.setText(Integer.toString(position + 1));

        return button;
    }

}