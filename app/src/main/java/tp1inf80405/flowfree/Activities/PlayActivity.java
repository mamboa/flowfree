package tp1inf80405.flowfree.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import tp1inf80405.flowfree.DB.DbHelper;
import tp1inf80405.flowfree.DB.JeuDB;
import tp1inf80405.flowfree.PartieJouable.Jeu;
import tp1inf80405.flowfree.PartieJouable.Partie;
import tp1inf80405.flowfree.PartieJouable.SingletonJeu;
import tp1inf80405.flowfree.R;
import tp1inf80405.flowfree.Terrain.Grille;
/**
 * Created by TP1 on 04/02/2016.
 */
public class PlayActivity extends AppCompatActivity {

    private Grille grille_;
    private TextView titreLabel_;
    private TextView coupsLabel_;
    private TextView casesLabel_;
    private Button recommencerButton_;
    private Button mPrevButton;
    private Button suivantButton_;
    private Partie partie_;
    private SingletonJeu singletonJeu_ = SingletonJeu.getInstance();
    private JeuDB jeuDB_ = new JeuDB(this);
    private DbHelper dbHelper = new DbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playactivity);

        int id = getIntent().getExtras().getInt("Id");
        Jeu jeu = singletonJeu_.obtenirJeuViaId(id);

        grille_ = (Grille) findViewById(R.id.playBoard);
        grille_.initialiserJeu(jeu);
        partie_ = new Partie(jeu);

        coupsLabel_ = (TextView) findViewById(R.id.coupsLabel);
        mAJTubes();
        casesLabel_ = (TextView) findViewById(R.id.casesLabel);
        mAJCases();

        titreLabel_ = (TextView)findViewById(R.id.titreLabel);
        titreLabel_.setText("Jeu " + singletonJeu_.obtenirNumeroJeu(partie_.obtenirJeu()));
        recommencerButton_ = (Button) findViewById(R.id.recommencerButton);
        recommencerButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reinitialiser();
            }
        });
        mPrevButton = (Button)findViewById(R.id.playPreviousButton);
        mPrevButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                partiePrecedente();
            }
        });
        suivantButton_ = (Button)findViewById(R.id.playNextButton);
        suivantButton_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                partieSuivante();
            }
        });
        affichageBoutons();
    }

    @Override
    protected void onResume() {
        super.onResume();
        grille_.chargerLesCouleurs();
        grille_.invalidate();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void update() {
        mAJTubes();
        mAJCases();
        int casesOccupees_ = grille_.nombreDeCasesOccupees();
        partie_.assignerCasesOccupees(casesOccupees_);
        //si une partie est terminee
        if(grille_.siTermine()) {
            //si gagne
            if(grille_.siVictoire()) {
                jeuDB_.update(partie_.obtenirJeu().obtenirId(), true);
                afficherVictoireDialog();
            }
            //si perdu
            else{
                afficherDefaiteDialog();
            }
        }
    }

    //quitter une partie
    @Override
    public void onBackPressed(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Quitter")
                .setMessage("Voulez-vous terminer cette partie?")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       finish();
                    }
                }) .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                    }
                });
        dialog.show();
    }

    /**
     * mAJTubes
     * mise à our des tubes restant
     */
    public void mAJTubes() {
        coupsLabel_.setText("Tubes : " + grille_.nombreTubesFormes());
    }

    /**
     * mAJCases
     * mise à jour des cases restantes
     */
    public void mAJCases() {
        casesLabel_.setText("Cases remplies: " + grille_.nombreDeCasesOccupees() + " / " + partie_.obtenirJeu().obtenirTaille() * partie_.obtenirJeu().obtenirTaille());
    }

    /**
     * afficherVictoireDialog
     * boite de dialoge a afficher en cas de victoire
     */
    private void afficherVictoireDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Fellicitations!")
                .setNegativeButton(R.string.rejouer, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                 reinitialiser();
                    }
                });

        final Jeu jeuSuivant = singletonJeu_.obtenirJeuSuivant(partie_.obtenirJeu());
        if(jeuSuivant != null) {
            dialog
                .setMessage("Vous avez gagné!!!")
                .setPositiveButton(R.string.jeuSuivant, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        partieSuivante();
                    }
                });
        }
        else {
            if(singletonJeu_.obtenirPremierJeuNiveauSuivant(partie_.obtenirJeu()) != null)
            {
                dialog
                        .setMessage("Vous avez gagné!!!\nCeci était la dernière partie de ce niveau 7x7.")
                        .setPositiveButton(R.string.niveauSuivant, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                partieSuivanteNiveauSuivant();
                            }
                        });
            }
            else{
            dialog
                .setMessage("Vous avez gagné!!!\nCeci était la dernière partie.")
                .setPositiveButton(R.string.quitter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        }
        dialog.show();
    }

    /**
     * afficherDefaiteDialog
     * boite de dialoge a afficher en cas de defaite
     */
    public void afficherDefaiteDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Vous avez perdu!")
                .setCancelable(false)
                .setPositiveButton(R.string.rejouer, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        reinitialiser();
                    }
                });
        dialog.show();
    }

    /**
     * affichageBoutons
     * permet d'afficher ou de cacher les boutons selon l'état des partie
     */
    private void affichageBoutons() {
        if(singletonJeu_.SiPremierJeuDeLaTaille(partie_.obtenirJeu())) {
            mPrevButton.setVisibility(View.INVISIBLE);
        }
        else {
            mPrevButton.setVisibility(View.VISIBLE);
        }
        //on fait disparaître "suivant" si la partie n'est pas gagnee ou si il s'agit de la derniere partie
        if(singletonJeu_.siDernierJeuDeLaTaille(partie_.obtenirJeu())){
                suivantButton_.setVisibility(View.INVISIBLE);
        }
        else {
            //le bouton next n'est disponible que si la partie courante est gagnée
            if(dbHelper.siTermine(partie_.obtenirJeu().obtenirId()))
                suivantButton_.setVisibility(View.VISIBLE);
        }
        //on fait disparaître "suivant" si la partie courante n'a jamais été gagnee
        if (!dbHelper.siTermine(partie_.obtenirJeu().obtenirId()))
            suivantButton_.setVisibility(View.INVISIBLE);
    }

    /**
     * reinitialiser
     * reinitialiser le terrain de jeu
     */
    private void reinitialiser() {
        partie_.reinitialiser();
        grille_.reinitialiser();
        grille_.invalidate();
        mAJTubes();
        mAJCases();
        affichageBoutons();
        titreLabel_.setText("Partie " + singletonJeu_.obtenirNumeroJeu(partie_.obtenirJeu()));
    }

    /**
     * partieSuivante
     * permet de charger la partie suivante
     */
    private void partieSuivante() {
        Jeu jeuSuivant = singletonJeu_.obtenirJeuSuivant(partie_.obtenirJeu());
        if(jeuSuivant != null) {
            partie_ = new Partie(jeuSuivant);
            grille_.initialiserJeu(jeuSuivant);
            grille_.invalidate();
            reinitialiser();
        }
    }

    /**
     * partieSuivanteNiveauSuivant()
     * nous renvoie à la premiere partie du niveau suivant
     */
    private void partieSuivanteNiveauSuivant(){
        Jeu jeuSuivant = singletonJeu_.obtenirPremierJeuNiveauSuivant(partie_.obtenirJeu());
        if(jeuSuivant != null) {
            //on nettoie la grille
            reinitialiser();
            //on redemarre l'activité comme si on avait cliqué de l'exterieur
            Intent intent = this.getIntent();
            intent.putExtra("Id", jeuSuivant.obtenirId());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.overridePendingTransition(0, 0);
            this.finish();
            this.overridePendingTransition(0, 0);
            startActivity(intent);
        }
    }

    /**
     * partiePrecedente()
     * Nous renvoie à la partie précedente
     */
    private void partiePrecedente() {
        Jeu jeuPrecedent = singletonJeu_.obtenirJeuPrecedent(partie_.obtenirJeu());
        if(jeuPrecedent != null) {
            partie_ = new Partie(jeuPrecedent);
            grille_.initialiserJeu(jeuPrecedent);
            reinitialiser();
        }
    }

}
