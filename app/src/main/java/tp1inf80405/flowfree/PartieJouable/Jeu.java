package tp1inf80405.flowfree.PartieJouable;


import java.util.ArrayList;

import tp1inf80405.flowfree.Terrain.Point;

/**
 * Cette classe définit les différents niveaux (3 par dimensions
 * de la grille selon l'énoncé). C'est lorsqu'elle est installée
 * qu'une partie peut débuter
 * Created by TP1 on 01/02/2016
 */
public class Jeu {

    private int nombreCouleurs_;
    private int id_;
    private ArrayList<Point> listePoints_;
    private int taille_;

    public Jeu(int id, int taille, ArrayList<Point> points) {
        id_ = id;
        taille_ = taille;
        listePoints_ = new ArrayList<Point>();
        listePoints_.addAll(points);
        nombreCouleurs_ = listePoints_.size() / 2;
    }

    public int obtenirNombreCouleurs() {
        return nombreCouleurs_;
    }

    public int obtenirId() {
        return id_;
    }

    public void assignerId(int id) {
        id_ = id;
    }

    public int obtenirTaille() {
        return taille_;
    }

    public ArrayList<Point> obtenirPoints() {
        return listePoints_;
    }


}
