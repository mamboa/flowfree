package tp1inf80405.flowfree.PartieJouable;
import java.util.List;


/**
 * Created by TP1 on 01/02/2016
 */

public class SingletonJeu {
    public List<Jeu> jeux_;
    public int taille7 = 0;
    public int taille8 = 0;

    private static SingletonJeu instance_ = new SingletonJeu();
    public static SingletonJeu getInstance() {
        return instance_;
    }

    private SingletonJeu(){}

    /**
     * obtenirJeuPrecedent
     * @param jeu le jeu courant
     * @return le jeu precedent
     */
    public Jeu obtenirJeuPrecedent(Jeu jeu) {
        int taille = jeu.obtenirTaille();
        int min;
        switch(taille) {
            case 7:
                min = 0;
                break;
            case 8:
                min = taille7;
                break;
            default:
                return null;
        }
        int id = jeu.obtenirId();
        if(id <= min) {
            return null;
        }
        return jeux_.get(id - 1);
    }

    /**
     * obtenirJeuSuivant
     * @param jeu le jeu courant
     * @return le jeu suivant
     */
    public Jeu obtenirJeuSuivant(Jeu jeu) {
        int size = jeu.obtenirTaille();
        int max;
        switch(size) {
            case 7:
                max = taille7 - 1;
                break;
            case 8:
                max = taille7 + taille8 - 1;
                break;
            default:
                return null;
        }
        int id = jeu.obtenirId();
        if(id >= max) {
            return null;
        }
        return jeux_.get(id + 1);
    }

   public Jeu obtenirPremierJeuNiveauSuivant(Jeu jeu){
       int size = jeu.obtenirTaille();
       int max = 0;
       switch(size){
           case 7:
               max = taille7 -1;
               break;
           //puisque le niveau 8 est le dernier
           case 8:
               return  null;

       }

       return jeux_.get(max + 1);
   }

    /**
     * obtenirJeuViaId
     * obtenir le jeut dont l'identifiant est donné en entré
     * @param id l'identifiant dont ont veut connaître le jeu
     * @return le jeu
     */
    public Jeu obtenirJeuViaId(int id) {
        return jeux_.get(id);
    }

    /**
     * SiPremierJeuDeLaTaille
     * @param jeu
     * @return
     */
    public boolean SiPremierJeuDeLaTaille(Jeu jeu) {
        int size = jeu.obtenirTaille();
        int min;
        switch(size) {
            case 7:
                min = 0;
                break;
            case 8:
                min = taille7;
                break;
            default:
                min = -1;
        }
        return jeu.obtenirId() == min;
    }

    /**
     * siDernierJeuDeLaTaille
     * @param jeu le jeu courant
     * @return
     */
    public boolean siDernierJeuDeLaTaille(Jeu jeu) {
        int taille = jeu.obtenirTaille();
        int max;
        switch(taille) {
            case 7:
                max = taille7 - 1;
                break;
            case 8:
                max = taille7 + taille8 - 1;
                break;
            default:
                max = -1;
        }
        return jeu.obtenirId() == max;
    }

    /**
     * obtenirNumeroJeu ()
     * le numero  = indiceJeu + 1
     * @param jeu jeu dont on veut avoir le numero
     * @return numeroJeu
     */
    public int obtenirNumeroJeu(Jeu jeu) {
        switch(jeu.obtenirTaille()) {
            case 7:
                return jeu.obtenirId() + 1;
            case 8:
                return jeu.obtenirId() - taille7 + 1;
            default:
                return -1;

        }
    }
}
